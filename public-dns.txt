1.1.1.1  # Cloudflare
1.0.0.1  # Cloudflare
1.0.0.3  # Cloudflare - Malware and Adult Blocking
1.1.1.3  # Cloudflare - Malware and Adult Blocking
1.0.0.2  # Cloudflare - Malware Blocking
1.1.1.2  # Cloudflare - Malware Blocking
8.26.56.26  # Comodo Secure
8.20.247.20  # Comodo Secure
8.8.4.4  # Google
8.8.8.8  # Google
4.2.2.1  # Level3
4.2.2.2  # Level3
4.2.2.3  # Level3
4.2.2.4  # Level3
4.2.2.5  # Level3
4.2.2.6  # Level3
129.250.35.250  # NTT
129.250.35.251  # NTT
208.67.222.222  # OpenDNS
208.67.220.222  # OpenDNS
208.67.222.123  # OpenDNS - Family Shield
208.67.220.123  # OpenDNS - Family Shield
208.67.222.2  # OpenDNS - Sandbox
208.67.220.2  # OpenDNS - Sandbox
9.9.9.9  # Quad9
149.112.112.112  # Quad9
9.9.9.11  # Quad9
9.9.9.11  # Quad9
9.9.9.10  # Quad9 - No Blocking
149.112.112.10  # Quad9 - No Blocking
204.117.214.10  # Sprintlink
199.2.252.10  # Sprintlink
64.6.64.6  # Verisign
64.6.65.6  # Verisign
